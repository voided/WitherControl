# WitherControl

> **Note**
>
> Repository migrated from `github:voidedWarranties/WitherControl`.

Plugin for disabling Wither block damage.
